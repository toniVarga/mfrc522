/*
 * mfrc522.h
 *
 *  Created on: 9. lis 2018.
 *      Author: Toni Varga
 */
#include <inttypes.h>

#ifndef __MFRC522__
#define __MFRC522__

#define MFRC_LOG_TAG						"MFRC522"

#define MFRC_SS_PIN							GPIO_NUM_15
#define MFRC_SCK_PIN						GPIO_NUM_14
#define MFRC_MISO_PIN						GPIO_NUM_12
#define MFRC_MOSI_PIN						GPIO_NUM_13
#define MFRC_RST_PIN						GPIO_NUM_32
#define MFRC_IRQ_PIN						GPIO_NUM_35
#define MFRC_SPI_HOST						HSPI_HOST

#define COMMAND_REG							0x01
#define FIFO_DATA_REG						0x09
#define FIFO_LEVEL_REG						0x0A
#define AUTOTEST_REG						0x36

#define IDLE								0b0000
#define MEM									0b0001
#define GENERATE_RAND_ID					0b0010
#define CALC_CRC							0b0011
#define TRANSMIT							0b0100
#define NO_CMD_CHANGE						0b0111
#define RECEIVE								0b1000
#define TRANSCEIVE							0b1100
#define MF_AUTHENT							0b1110
#define SOFT_RESET							0b1111

#define FIFO_BUFFER_SIZE					64

void mfrc_init(void);
void mfrc_self_test(void);

#endif
