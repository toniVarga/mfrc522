/*
 * mfrc522.c
 *
 *  Created on: 9. lis 2018.
 *      Author: Toni Varga
 */

#include "mfrc522.h"
#include "driver/spi_master.h"
#include <esp_log.h>
#include "esp_types.h"
#include "esp_heap_caps.h"
#include <string.h>
#include <driver/gpio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

spi_device_handle_t spi;

void spi_init(void) {
	esp_err_t ret;

	spi_bus_config_t buscfg={
		.miso_io_num=MFRC_MISO_PIN,
		.mosi_io_num=MFRC_MOSI_PIN,
		.sclk_io_num=MFRC_SCK_PIN,
		.quadwp_io_num=-1,
		.quadhd_io_num=-1,
	};

	spi_device_interface_config_t devcfg={
		.clock_speed_hz=10*1000,
		.mode=0,
		.spics_io_num=MFRC_SS_PIN,
		.queue_size=1
	};

	//Initialize the SPI bus
	ret=spi_bus_initialize(MFRC_SPI_HOST, &buscfg, 1);
    if(ret != ESP_OK) {
    	ESP_LOGE(MFRC_LOG_TAG, "spi_bus_initialize failed: %s", esp_err_to_name(ret));
    }
	//Attach the MFRC522 to the SPI bus
	ret=spi_bus_add_device(MFRC_SPI_HOST, &devcfg, &spi);
    if(ret != ESP_OK) {
    	ESP_LOGE(MFRC_LOG_TAG, "spi_bus_add_device failed: %s", esp_err_to_name(ret));
    }

    //https://docs.espressif.com/projects/esp-idf/en/latest/api-reference/system/mem_alloc.html
}

void mfrc_write(uint8_t* command_buffer, uint8_t command_buffer_len) {
    esp_err_t ret;
    spi_transaction_t t;

    //Command must meet specification. Here is the handling
    *command_buffer = (*command_buffer<< 1);

    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=command_buffer_len*8;                 //Len is in bytes, transaction length is in bits.
    t.tx_buffer=command_buffer;               //Data
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    if(ret != ESP_OK) {
    	ESP_LOGE(MFRC_LOG_TAG, "spi_bus_add_device failed: %s", esp_err_to_name(ret));
    }
}

void mfrc_read(uint8_t* command_buffer, uint8_t command_buff_len, uint8_t* read_data_buff, uint8_t read_buff_len) {
    esp_err_t ret;
    spi_transaction_t t;

    //Command must meet specification. Here is the handling
    *read_data_buff = (*read_data_buff << 1) | (1<<7);

    memset(&t, 0, sizeof(t));       //Zero out the transaction
    t.length=(command_buff_len+read_buff_len)*8;                 //Len is in bytes, transaction length is in bits.
    t.rx_buffer=read_data_buff;               //Data
    t.tx_buffer=command_buffer;
    ret=spi_device_polling_transmit(spi, &t);  //Transmit!
    if(ret != ESP_OK) {
    	ESP_LOGE(MFRC_LOG_TAG, "spi_bus_add_device failed: %s", esp_err_to_name(ret));
    }

}

/**
 * @brief Initializes spi bus for mfrc 522
 *
 * @param void
 *
 * @return void
 */
void mfrc_init(void) {
	spi_init();
}

/**
 * @brief Performs a self test for mfrc522 module and outputs results to console
 *
 * @param void
 *
 * @return void
 */
void mfrc_self_test(void) {
    spi_transaction_t t;
	uint8_t addr = (0x02 << 1) | (1<<7);
	//uint8_t* rx_buff = (uint8_t*)heap_caps_malloc(1, MALLOC_CAP_DMA);

    memset(&t, 0, sizeof(t));
    t.length=1*8;
    t.rxlength = 1;
    t.flags = SPI_TRANS_USE_RXDATA | SPI_TRANS_USE_TXDATA;
    t.tx_data[0]=addr;
    puts("Starting spi transaction\n");
    spi_device_polling_transmit(spi, &t);  //Transmit!
    printf("SPI transaction ended\n");
    //printf("Data: %x", t.rx_data[0]);
    printf("Data: 25");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    printf("ENDED!!");

	/*
	 	 1. Perform a soft reset.
		2. Clear the internal buffer by writing 25 bytes of 00h and implement the Config
		command.
		3. Enable the self test by writing 09h to the AutoTestReg register.
		4. Write 00h to the FIFO buffer.
		5. Start the self test with the CalcCRC command.
		6. The self test is initiated.
		7. When the self test has completed, the FIFO buffer contains the following 64 bytes:
		FIFO buffer byte values for MFRC522 version 1.0:
		00h, C6h, 37h, D5h, 32h, B7h, 57h, 5Ch,
		C2h, D8h, 7Ch, 4Dh, D9h, 70h, C7h, 73h,
		10h, E6h, D2h, AAh, 5Eh, A1h, 3Eh, 5Ah,
		14h, AFh, 30h, 61h, C9h, 70h, DBh, 2Eh,
		64h, 22h, 72h, B5h, BDh, 65h, F4h, ECh,
		22h, BCh, D3h, 72h, 35h, CDh, AAh, 41h,
		1Fh, A7h, F3h, 53h, 14h, DEh, 7Eh, 02h,
		D9h, 0Fh, B5h, 5Eh, 25h, 1Dh, 29h, 79h
		FIFO buffer byte values for MFRC522 version 2.0:
		00h, EBh, 66h, BAh, 57h, BFh, 23h, 95h,
		D0h, E3h, 0Dh, 3Dh, 27h, 89h, 5Ch, DEh,
		9Dh, 3Bh, A7h, 00h, 21h, 5Bh, 89h, 82h,
		51h, 3Ah, EBh, 02h, 0Ch, A5h, 00h, 49h,
		7Ch, 84h, 4Dh, B3h, CCh, D2h, 1Bh, 81h,
		5Dh, 48h, 76h, D5h, 71h, 061h, 21h, A9h,
		86h, 96h, 83h, 38h, CFh, 9Dh, 5Bh, 6Dh,
		DCh, 15h, BAh, 3Eh, 7Dh, 95h, 03Bh, 2Fh
	 * */
}
